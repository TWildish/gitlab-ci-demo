# Gitlab-CI-demo
This is a simple repository for demonstrating how to pull from **bitbucket** and push to **gitlab**.

The code here, **hello.c**, **Makefile** and **Dockerfile** are all the same as in the tutorial repository at
Gitlab. However, this time, we're going to clone this repository from **bitbucket**, then set up dual remotes, so we can pull from **bitbucket**, and push to **gitlab**.

This lets us continue the main development in **bitbucket**, where we're used to having the code, but still use the CI features of **gitlab**.

First, clone this repository in bitbucket. Go to https://bitbucket.org/TWildish/gitlab-ci-demo/, click the **Actions** button (top-left, the button with three horizonally aligned dots), and select **Fork**. In a few seconds, you will have your own copy of this repository.

Now clone your fork to your local disk. Let's call this the **'gitlab clone'**:

```
git clone git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git
cd gitlab-ci-demo
```

Next, create a new project in gitlab. Give it the same name, for convenience, but don't put anything in it yet.

In your bitbucket clone, set the gitlab repository as a destination for 'git push'

```
> git remote set-url --add origin git@gitlab.com:YOUR_USERNAME/gitlab-ci-demo.git
> git remote set-url --push origin git@gitlab.com:YOUR_USERNAME/gitlab-ci-demo.git
> git remote --verbose show
origin  git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git (fetch)
origin  git@gitlab.com:YOUR_USERNAME/gitlab-ci-demo.git (push)
```

You see that the **fetch** will be done from bitbucket, and the **push** will go to gitlab.

Now push the code to gitlab for the first time:

```
git push
```

If you go to gitlab.com, look at the 'Pipelines' page for this project, you should see a build is starting!

Now you have another problem: how do you push changes back to bitbucket, as well as to gitlab? There are at least two solutions. The first we'll try is to check out the bitbucket repository somewhere else, and make all your edits there, as you would normally. Then, in your clone that pushes to gitlab, you can pull those changes before pushing them to gitlab.

So, go to a new directory somewhere else (e.g. **/tmp**) and check out your bitbucket clone again. We'll call this copy the **'bitbucket clone'**:

```
cd /tmp
git clone git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git
cd gitlab-ci-demo
```

Now make a change to the code, e.g. add a new file:

```
> ls -l > MANIFEST.txt
> git add MANIFEST.txt
> git commit -m 'added a MANIFEST file'
[master 700416e] added a MANIFEST file
 1 file changed, 6 insertions(+)
 create mode 100644 MANIFEST.txt
> git push
```

Next, go back to the **gitlab clone** directory, **pull** the new code from bitbucket, and **push** it to gitlab:

```
> git pull
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From bitbucket.org:YOUR_USERNAME/gitlab-ci-demo
   554291e..76676e5  master     -> origin/master
Updating 554291e..76676e5
Fast-forward
 MANIFEST.txt | 7 +++++++
 1 file changed, 7 insertions(+)
 create mode 100644 MANIFEST.txt

> git push
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 424 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To gitlab.com:TonyWildish/gitlab-ci-demo.git
   554291e..76676e5  master -> master
```

That's all! Now you can track changes from bitbucket and push them to gitlab.

++ An alternative way of doing it
Instead of having two clones, one for developing and pushing to bitbucket, the other for simply mirroring bitbucket to gitlab, you can use the same clone for both functions. Here's how:

First, clone the bitbucket repository

```
> git clone git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git
Cloning into 'gitlab-ci-demo'...
remote: Counting objects: 44, done.
remote: Compressing objects: 100% (41/41), done.
remote: Total 44 (delta 21), reused 0 (delta 0)
Receiving objects: 100% (44/44), 6.17 KiB | 0 bytes/s, done.
Resolving deltas: 100% (21/21), done.
> cd gitlab-ci-demo/

> git remote --verbose show
origin  git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git (fetch)
origin  git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git (push)
```

The last bit there shows how you can both **pull** (fetch) from and **push** to the bitbucket repository. This is the default when you clone a repository.

Next, create an empty gitlab repository, and define it to be a remote repository for your bitbucket clone. That takes two steps, because adding the gitlab remote seems to overwrite the default remote:
- add the gitlab repository as a remote push destination
- re-add the original bitbucket repository as a remote destination

```
> git remote set-url --push --add origin git@gitlab.com:YOUR_USERNAME/gitlab-ci-demo.git
> git remote --verbose show
origin  git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git (fetch)
origin  git@gitlab.com:YOUR_USERNAME/gitlab-ci-demo.git (push)
```

See that now the **fetch** and **push** URLs are different. We need to add back the bitbucket URL for pushing.

```
> git remote set-url --push --add origin git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git
> git remote --verbose show
origin  git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git (fetch)
origin  git@gitlab.com:YOUR_USERNAME/gitlab-ci-demo.git (push)
origin  git@bitbucket.org:YOUR_USERNAME/gitlab-ci-demo.git (push)
```

So now, whenever we **git pull**, we fetch from bitbucket. And whenever we **git push**, we push to **both** bitbucket and gitlab, at the same time!


N.B. For more information on working with remotes, check section "2.5 Git Basics - Working with Remotes" in ‘Pro Git’, at https://git-scm.com/book/en/v2.
